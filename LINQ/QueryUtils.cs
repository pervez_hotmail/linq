﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    public static class QueryUtils
    {

        public static Query And(this Query first, Query second)
        {
            return new Query(first.Intersect(second));
        }

        public static Query Or(this Query first, Query second)
        {
            return new Query(first.Union(second));
        }

        public static Query Not(this Query first, Query second)
        {
            return new Query(first.Intersect(second));
        }
    }
}
