﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    static class ListUtils
    {
        public static List<T> Push<T>(this List<T> list, T e)
        {
            list.Add(e);
            return list;
        }
    }
}
