﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace LINQ
{
    internal class Book
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
    }

    static class EnumerableUtils
    {
        public static void Print<T>(this IEnumerable<T> list, string delimiter = " ")
        {
            Console.WriteLine(string.Join(delimiter, list.Select(e => e.ToString())));
        }

        public static void Print<TArg, TResult>(this IEnumerable<TArg> list, Func<TArg, TResult> func, string delimiter = " ")
        {
            Console.WriteLine(string.Join(delimiter, list.Select(e => func(e).ToString())));
        }
    }


    internal class Program
    {

        public static IEnumerable<T> Flatten<T>(IEnumerable<T[]>  list)
        {
            foreach (var all in list)
                foreach (var e in all)
                    yield return e;
        }

        private static void Main(string[] args)
        {

            //var evens = new[] {2, 4, 6, 8, 10};
            //var odds = new[] {1, 3, 5, 7, 9};

            //var pairs = from odd in odds
            //            from even in evens
            //            select new {First = odd, Second = even};

            //var pairs2 = from odd in odds
            //             join even in evens
            //                 on odd equals even - 1
            //             select new {First = odd, Second = even};
            //pairs2.Print();
            ////pairs.Print("\n");

            //using (var ctx = new NorthwindDataContext())
            //{
            //    ctx.Log = Console.Out;
            //    var categories = ctx.Categories.ToList();
            //    var products = ctx.Products.ToList();

            //    var categoryProducts2 = from category in categories
            //                            from product in products
            //                            where category.CategoryID == product.CategoryID
            //                            select new { category.CategoryName, product.ProductName };

            //    var categoryProducts = from category in categories
            //                           join product in products
            //                           on category.CategoryID equals product.CategoryID
            //                           select new { category.CategoryName, product.ProductName };
                

            //    categoryProducts.ToList();
            //    categoryProducts2.ToList();

            //    //categoryProducts.Print("\n");

            //}

            //var q = new QuerySource(new StreamReader("abc.txt"));

            //var helloAndWorldOrUniverse =q.Word("hello").And(q.Word("world").Or(q.Word("universe")));

            //const int x = 5, y = 30;
            //Enumerable.Range(1, y).Aggregate(1, (pow, e) => pow*x);

            //var list = new[] {11, 12, 13, 14, 15, 16, 17, 18, 19, 20};

            //var list = Enumerable.Range(11, 10);

            //var fact = list.Aggregate(1, (acc, e) => acc + e);
            //var sum = list.Sum(); 
            //var max = list.Max();
            //var min = list.Min();
            //var evensCount= list.Count(e => e % 2 == 0);

            //var arr = list.ToArray();
            //var myList = list.ToList();

            //var books = new List<Book>
            //                {
            //                    new Book {ID = 3, Title = "The Java Programming Language", Price = 35.90},
            //                    new Book {ID = 4, Title = "The Ruby Programming Language", Price = 45.99},
            //                    new Book {ID = 1, Title = "The C# Programming Language", Price = 38.90},
            //                    new Book {ID = 2, Title = "The C++ Programming Language", Price = 29.90},
            //                };


            //var arr = new[] {1, 2, 3, 4};
            //var arr2 = new[] {3, 4, 5, 6};

            //arr.Except(arr2).Print();

            //const int n = 6;

            //Console.WriteLine(Enumerable.Range(1, n - 1).Where(e => n%e == 0).Sum() == n);



            //var dict = books.ToDictionary(b => b.ID, b => b);
            //foreach(var kv in dict)
            //    Console.WriteLine(kv.Key + " " + kv.Value);

            //var programmingBooks = from book in books 
            //            where book.Title.Contains("Programming") 
            //            select book;

            //var books2 = books
            //        .Where(b => b.Title.Contains("Programming"))
            //        .OrderBy(b => b.Title)
            //        .Select(b => new { b.Title, b.Price });

            //var iter = books2.GetEnumerator();
            //while (iter.MoveNext())
            //    Console.WriteLine(iter.Current.Title);

            //var titles = books.Select(book => book.Title);

            //var titles2 = from book in books 
            //              select book.Title;

            //var fb = new {Foo = 1, Bar = "Hello"};
            //Console.WriteLine(fb.Foo);
            //Console.WriteLine(fb.Bar);
            //var booksForPrice35 = books.Where(book => book.Price > 35.0).Select(book => new { book.Title, book.Price});

            //foreach(var bi in booksForPrice35)
            //    Console.WriteLine(bi.Title + " " + bi.Price);
            //var titlesForPrice35_2 = from book in books where book.Price > 35 select book.Title;

            //var booksByTitle = books.Where(b => b.Price > 35).OrderBy(b => b.Title).ThenByDescending(b => b.Price);

            //var booksByTitle2 = from book in books
            //                   where book.Price > 35
            //                   orderby book.Title, book.Price descending
            //                   select book; //MUST

            // select book.title from books as book


            //var lines = File.ReadAllLines("");
            //var allWords = new List<string[]>();
            //foreach (var line in lines)
            //{
            //    var wds = line.Split();
            //    allWords.Add(wds);
            //}

            //var list = new List<string>();
            //foreach(var words in allWords)
            //    foreach(var word in words)
            //        list.Add(word);

            var seperators = new[] { ' ', '\t', '\n', '{', '}', '(', ')', '[', ']', '=', ',' };
            var lines = new StreamReader(@"C:\Users\pervez\documents\visual studio 2012\Projects\LINQ\LINQ\Program.cs").ReadLines().ToList();

            var wordsToLineNos = lines.Select((Line, LineNo) => new {Line, LineNo = LineNo + 1})
                //Associate line with line no
                .SelectMany(pair => pair.Line.Split().Select(Word => new {Word, pair.LineNo}))
                //Associate word with its line no after splitting line into line nos
                .ToLookup(p => p.Word, p => p.LineNo);

            foreach (var e in wordsToLineNos)
            {
                Console.Write(e.Key + " : ");
                foreach(var lineNo in e)
                    Console.Write(lineNo + " ");
                Console.WriteLine();
            }


            //var words = from line in lines
            //            from word in line.Split(seperators)
            //            select word;





            //var listOfLists = new[] {new[] {1, 2, 3}, new[] {4, 5, 6}, new[] {7, 8, 9}};
            //var list = listOfLists.SelectMany(e => e);
            //foreach(var e in list)
            //    Console.WriteLine(e);
        }
    }
}