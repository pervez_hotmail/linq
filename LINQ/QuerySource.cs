﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    public class QuerySource
    {
        private readonly List<string> lines = new List<string>(); 
        private readonly Dictionary<string, List<int>> wordsToLineNos = new Dictionary<string, List<int>>(); 

        public QuerySource(TextReader reader)
        {
            lines = reader.ReadLines().ToList();

            var wordsToLineNos = lines.Select((Line, LineNo) => new {Line, LineNo = LineNo + 1}) //Associate line with line no
                .SelectMany(pair => pair.Line.Split().Select(Word => new {Word, pair.LineNo})) //Associate word with its line no after splitting line into line nos
                .ToLookup(pair => pair.Word);

            foreach(var e in wordsToLineNos)
            {
                Console.WriteLine(e.Key);
                e.Print(",");
            }
        }

        public string this[int index]
        {
            get { return lines[index]; }
        }

        public Query Word(string word)
        {
            return new Query(wordsToLineNos[word]);
        }

        public int LineCount
        {
            get { return lines.Count; }
        }
    }
}
