using System;
using System.IO;

static internal class FileUtils
{
    public static string[] ReadLines(this TextReader reader)
    {
        return reader.ReadToEnd().Split(new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);
    }
}