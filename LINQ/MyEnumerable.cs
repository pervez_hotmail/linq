﻿using System;
using System.Collections.Generic;

namespace LINQ
{
    public static class MyEnumerable
    {
        //public static IEnumerable<TResult> Select<TArg, TResult>(this IEnumerable<TArg> list, Func<TArg, TResult>  func)
        //{
        //    var iter = list.GetEnumerator();
        //    while (iter.MoveNext())
        //        yield return func(iter.Current);
        //}

        //public static IEnumerable<TArg> Where<TArg>(this IEnumerable<TArg> list, Func<TArg, bool>  func)
        //{
        //    var iter = list.GetEnumerator();
        //    while (iter.MoveNext())
        //        if (func(iter.Current))
        //            yield return iter.Current;
        //}

        //public static List<TArg> OrderBy<TArg, TElement>(this IEnumerable<TArg> list, Func<TArg, TElement> func)
        //{
        //    var dict = new SortedDictionary<TElement, TArg>();
        //    foreach(var e in list)
        //        dict.Add(func(e), e);
        //    return new List<TArg>(dict.Values);
        //}

        public static void Each<T>(this IEnumerable<T> list, Action<T> action )
        {
            foreach (var e in list)
                action(e);
        }
    }
}
