﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    public class Query : IEnumerable<int>
    {
        private readonly IEnumerable<int> lines;

        public Query(IEnumerable<int> lines)
        {
            this.lines = lines;
        }

        public IEnumerator<int> GetEnumerator()
        {
            return lines.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
