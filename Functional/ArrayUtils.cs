using System;
using System.Collections.Generic;

namespace Functional
{
    class ArrayUtils
    {
        public static T Max<T>(T first, params T[] rest)
            where T : IComparable<T>
        {
            var max = first;
            foreach (var e in rest)
                if (max.CompareTo(e) < 0)
                    max = e;
            return max;
        }

        public static int IndexOf<T>(T[] arr, T e, int pos = 0)
        {
            for (var i = pos; i < arr.Length; ++i)
                if (arr[i].Equals(e))
                    return i;
            return -1;
        }

        public static IEnumerable<int> Find<T>(T[] arr, T e)
        {
            var pos = -1;
            while((pos = IndexOf(arr, e, pos + 1)) != -1)
            yield return pos;
        }

    }
}