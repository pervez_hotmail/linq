﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Seartipy;

namespace Functional
{
    class FunctionalExamples
    {
        static void Main()
        {
            const int n = 5;
            
            var nos = EnumerableUtils.Range(1, n + 1);
            var fact = nos.Reduce(1, (acc, e) => acc*e);
            Console.WriteLine(fact);

            int[] arr = {25, 10, 35, 1};

            Console.WriteLine(arr.Reduce(Math.Max));
            Console.WriteLine(arr.Reduce(Math.Min));

            Console.ReadKey();
        }
    }
}
