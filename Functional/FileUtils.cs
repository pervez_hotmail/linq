﻿using System.Collections.Generic;
using System.IO;
using Seartipy;

namespace Functional
{
    class FileUtils
    {
        static void Main()
        {
            //var filename = @"C:\Users\pervez\documents\visual studio 2012\Projects\Functional\Functional\FileUtils.cs";
        }

        public static SortedDictionary<string, int> WordFrequency(string filename)
        {
            var text =
                File.ReadAllText(filename);

            var seperators = new[] {' ', '\n', '\t', '(', ')', ':', '.', '=', '<', '>', ',', ':', ';', '{', '}'};

            var words = text.Split(seperators);

            var lowerCaseWords = words.Map(word => word.ToLower());

            var wordFreq = new SortedDictionary<string, int>();
            
            
            lowerCaseWords.Each(word => wordFreq[word]  = wordFreq.ContainsKey(word) ? wordFreq[word] + 1 : 1);
            return wordFreq;
        }

        public static IEnumerable<string> Grep(string filename, string str)
        {
            return File.ReadLines(filename)
                .Map(line => line.Trim().ToLower())
                .Filter(line => line != "")
                .Filter(line => line.Contains(str));
            
        }
    }
}
