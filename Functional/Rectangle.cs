﻿
namespace Functional
{
    struct MyDateTime
    {
        private readonly int year;
        private readonly int month;
        private readonly int day;
        private readonly int hour;
        private readonly int min;
        private readonly int sec;
        private readonly int msec;
        private readonly int nsec;

        public MyDateTime(int year = 2012, int month = 1, int day = 1, 
            int hour = 0, int min = 0, int sec = 0, 
            int msec = 0, int nsec = 0)
        {
            this.year = year;
            this.month = month;
            this.day = day;
            this.hour = hour;
            this.min = min;
            this.sec = sec;
            this.msec = msec;
            this.nsec = nsec;
        }

        public MyDateTime(int year, int month, int day) : this()
        {
            this.year = year;
            this.month = month;
            this.day = day;
        }

        public int Year
        {
            get { return year; }
        }

        public int Month
        {
            get { return month; }
        }

        public int Day
        {
            get { return day; }
        }

        public int Hour
        {
            get { return hour; }
        }

        public int Min
        {
            get { return min; }
        }

        public int Sec
        {
            get { return sec; }
        }

        public int Msec
        {
            get { return msec; }
        }

        public int Nsec
        {
            get { return nsec; }
        }
    }
    class Rectangle
    {
        private readonly int left;
        private readonly int top;
        private readonly int width;
        private readonly int height;

        public Rectangle(int left, int top, int width, int height)
        {
            this.left = left;
            this.top = top;
            this.width = width;
            this.height = height;
        }

        public int Left
        {
            get { return left; }
        }

        public int Top
        {
            get { return top; }
        }

        public int Width
        {
            get { return width; }
        }

        public int Height
        {
            get { return height; }
        }

        static void Main()
        {
            var r = new Rectangle(width: 3, height: 4, left: 1, top: 2);

            var time = new MyDateTime(hour: 1, min: 5, sec: 15);
            var date = new MyDateTime(day: 1, month: 12, year: 2010);
        }
    }
}
