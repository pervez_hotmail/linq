﻿using System.Collections.Generic;

namespace Functional
{
    class Array<T>
    {

        
        private readonly Queue<T> q = new Queue<T>();

        public void Push(params T[] arr) 
        {
            foreach(var e in arr)
                q.Enqueue(e);
        }
    
    }


    
}
