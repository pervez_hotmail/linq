﻿using System;
using System.Collections.Generic;
using System.Linq;
using Seartipy;

namespace Functional
{
    internal class Book
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
    }

    public static class IntUtils
    {
        public static int Abs(this int x)
        {
            return x > 0 ? x : -x;
        }
    }

    class Program
    {
        

        public static void Foo(Func<int, int> func)
        {
            Console.WriteLine(func(10));
        }

        public static Func<int, int> Bind(Func<int, int, int> binaryFunc, int arg)
        {
            return x => binaryFunc(arg, x);
        }

        public static int Add(int x, int y)
        {
            return x + y;
        }
        static void Main()
        {
            var foo = 100.Abs();
            Func<int, int, int> add = Add;

            add(1, 2);

            var inc = Bind(Add, 1);
            Console.WriteLine(inc(10));

            var arr = new HashSet<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

            var strs = new List<string> { "HelLo", "woRLd", "uNiverSe" };

            Console.WriteLine(arr.Contains(x => x % 2 == 0));
            Console.WriteLine(strs.Contains(s => s.ToLower() == "world"));

            strs.Each(Console.WriteLine);

            arr.Filter(x => x % 2 == 0).Print();

            strs.Map(s => s.ToLower()).Print();

            Console.WriteLine( EnumerableUtils.Sum(arr) );


            var books = new List<Book>
            {
                new Book {ID = 1, Title = "The C# Programming Language", Price = 38.90},
                new Book {ID = 2, Title = "The C++ Programming Language", Price = 29.90},
                new Book {ID = 3, Title = "The Java Programming Language", Price = 35.90}
            };

            var titles = books.Select(book => book.Title);
        }
    }

    
}
