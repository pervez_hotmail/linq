﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seartipy
{
    public static class EnumerableUtils
    {
        public static IEnumerable<int> Range(int start, int stop)
        {
            for (var i = start; i < stop; ++i)
                yield return i;
        }

        public static int IndexOf<T>(this IEnumerable<T> list, Func<T, bool> pred)
        {
            var index = 0;
            foreach (var e in list)
            {
                if (pred(e))
                    return index;
                ++index;
            }
            return -1;
        }

        public static bool Contains<T>(this IEnumerable<T> list, Func<T, bool> pred)
        {
            return IndexOf(list, pred) != -1;
        }

        public static void Each<T>(this IEnumerable<T> arr, Action<T> action)
        {
            foreach (var e in arr)
                action(e);
        }

        public static void Print<T>(this IEnumerable<T> arr)
        {
            Each(arr, x => Console.WriteLine(x));
        }

        public static IEnumerable<T> Filter<T>(this IEnumerable<T> list, Func<T, bool> pred)
        {
            foreach(var e in list)
                if(pred(e))
                    yield return e;
        }

        public static IEnumerable<T2>  Map<T1, T2>(this IEnumerable<T1> list, Func<T1, T2> func)
        {
            foreach (var e in list)
                yield return func(e);
        }

        public static T Reduce<T>(this IEnumerable<T> arr, T seed, Func<T, T, T> func)
        {
            var sum = seed;
            foreach (var e in arr) // 1 + 2 + 3 + 4 + 5
                sum = func(sum, e); // 0 + 1 + 2 + 3 + 4 + 5
            /*
             * 0 + 1 == 1
             * 1 + 2 == 3
             * 3 + 3 == 6
             * 7 + 4 == 10
             * 10 + 5 == 15
             */
            return sum;
        }

        public static T Reduce<T>(this IEnumerable<T> list, Func<T, T, T> func)
        {
            var iter = list.GetEnumerator();
            iter.MoveNext();
            var acc = iter.Current;

            while (iter.MoveNext())
                acc = func(acc, iter.Current);
            return acc;
        }

        public static int Sum(this IEnumerable<int> list)
        {
            return Reduce(list, 0, (acc, e) => acc + e);
        }
    }
}
