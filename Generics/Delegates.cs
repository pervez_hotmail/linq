﻿using System;

namespace Functional
{
    delegate void MyBinaryAction(int x, int y);
    public class Delegates
    {
        public static void Add(int x, int y)
        {
            Console.WriteLine(x + y);
        }

        public static void Sub(int x, int y)
        {
            Console.WriteLine(x - y);
        }


        public static void Mul(int x, int y)
        {
            Console.WriteLine(x * y);
        }


        public static void Div(int x, int y)
        {
            Console.WriteLine(x / y);
        }

        static void Main()
        {
            Console.WriteLine("HELLO WORLD");
            Action<int, int> action = null;
            action += Add;
            action += Sub;
            action += Mul;
            action += Div;

            action(10, 2);

            action -= Sub;

            action(10, 2);

            MyBinaryAction action2 = new MyBinaryAction(action);
            action2(100, 2);

        }
    }
}
