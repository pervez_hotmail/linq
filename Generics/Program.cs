﻿using System;
using System.Collections.Generic;

namespace Generics
{
    class Person
    {
        public void F()
        {
            
        }
    }

    class Customer : Person
    {
        public void G()
        {}
    }

    class Employee : Person
    {
        public void H()
        {
            
        }
    }

    class Utils
    {
        
        public static void PrintAll(IEnumerable<Person> list)
        {
            foreach(var e in list)
                Console.WriteLine(e);
        }
    }

    delegate void MyAction<TArg>(TArg d);
    delegate TResult MyFunc<TResult>();

    class Program
    {

        
        static void BaseMethod(Person b)
        {
            b.F();
        }

        static Customer DerivedMethod()
        {
            return null;
        }

        static void Main(string[] args)
        {
            var foo = new Foo();
            foo.F();
            foo.G();

            var customers = new List<Customer>();

            customers.Add(new Customer());


            IEnumerable<Customer> persons = customers;
            Utils.PrintAll(persons);

            //var baseArr = new Person[3];
            //baseArr[0] = new Person();
            //baseArr[1] = new Customer();
            //baseArr[2] = new Person();

            //var derivedArr = new Customer[3];
            //derivedArr[0] = new Customer();
            //derivedArr[1] = new Customer();
            //derivedArr[2] = new Customer();

            //Person[] baseArr2 = derivedArr;

            //baseArr2[1] = new Person();

            //derivedArr[1].G();
        }
    }
}
