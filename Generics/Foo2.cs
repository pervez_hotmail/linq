﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Generics
{
    partial class Foo
    {
        partial void H();

        public void F()
        {
            H();
        }
    }
}
