﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Generics
{
    interface ISet<in T> 
    {
        void Set(T e);
    }

    interface IGet<out T> 
    {
        T Get();
    }

    interface IFooBar<T> 
    {
        void Set(T e);
        T Get();
    }

    
    class Variance
    {
        
        static void F(ISet<Customer> set)
        {
            set.Set(new Customer());
        }

        static void G(IGet<Person> get)
        {
            var b = get.Get();
            b.F();
        }


        static void Abc(Person p)
        {
            
        }

        static Customer Def()
        {
            return new Customer();
        }

        static void Main()
        {
            ISet<Customer> cs = null;
            ISet<Employee> es = null;
            ISet<Person> ps = null;

            cs.Set(new Customer());
            

            ps.Set(new Person());

            ps.Set(new Customer());

            cs = ps;

            F(ps);
        }
    }
}
